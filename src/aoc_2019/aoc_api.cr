require "http/client"
require "file_utils"

module AOC2019
  class AOCApi
    YEAR = 2019

    def initialize(@session_id : String)
      @client = HTTP::Client.new("adventofcode.com", tls: true)
      @cache_dir = File.join(
        ENV.fetch("XDG_CACHE_HOME", File.join(ENV["HOME"], ".cache")),
        "aoc_#{YEAR}"
      )
      FileUtils.mkdir_p(@cache_dir)
      @client.before_request do |request|
        request.cookies["session"] = @session_id
      end
    end

    def input(day)
      cached_input_path = File.join(@cache_dir, day.to_s)

      IO::Memory.new.tap do |io|
        if File.exists?(cached_input_path)
          File.open(cached_input_path) { |file| IO.copy(file, io) }
        else
          @client.get("/#{YEAR}/day/#{day}/input") do |response|
            unless response.success?
              raise "#{response.status_code}: #{response.body_io.gets_to_end}"
            end

            File.open(cached_input_path, "w+") do |file|
              IO.copy(response.body_io, IO::MultiWriter.new(io, file))
            end
          end
        end

        io.rewind
      end
    end
  end
end
