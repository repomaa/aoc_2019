require "colorize"

module AOC2019
  class Intcode
    class_property last_id = 1_u8
    enum Opcodes
      Add = 1
      Mul = 2
      Input = 3
      Output = 4
      JumpIfTrue = 5
      JumpIfFalse = 6
      LessThan = 7
      Equals = 8
      AdjustRelativeBase = 9
      Halt = 99
    end

    enum ParameterModes
      Position = 0
      Immediate = 1
      Relative = 2
    end

    class Halt < Exception
    end

    class Program
      protected setter memory

      def initialize(@memory : Array(Int64))
      end

      delegate size, to: @memory

      def [](index)
        @memory[index]? || 0_i64
      end

      def []=(index, value)
        if index >= @memory.size
          @memory.concat(Array.new(index + 1 - @memory.size, 0_i64))
        end

        @memory[index] = value
      end

      def values_at(*indexes : *T) forall T
        {% begin %}
          {
            {% for i in 0...T.size %}
              self[indexes[{{i}}]],
            {% end %}
          }
        {% end %}
      end

      def clone
        Program.new(@memory.clone)
      end

      def self.parse(input)
        memory = input.gets.not_nil!.split(',').map(&.to_i64)
        new(memory)
      end
    end

    @initial_state : Program
    getter program : Program
    @ip : UInt64
    @id : UInt8

    getter? halted

    private macro debug(string)
      {% unless flag?(:release) %}
        STDERR.print(({{string}}).colorize(Colorize::Color256.new(@id)))
      {% end %}
    end

    def self.new(program_input : IO, input : String = "", output : IO = STDOUT)
      new(program_input, IO::Memory.new(input), output)
    end

    def initialize(program_input : IO, @input : IO = IO::Memory.new, @output : IO = STDOUT)
      @id = self.class.last_id
      self.class.last_id += 1
      @initial_state = Program.parse(program_input)
      @program = @initial_state.clone
      @ip = 0_u64
      @relative_base = 0_i64
      @halted = false
    end

    def reset!
      debug "Resetting\n"
      @program = @initial_state.clone
      @ip = 0
      @relative_base = 0_i64
      @halted = false
    end

    def execute
      debug "Starting execution of program containing #{@program.size} ints\n"
      loop { execute_instruction(*parse_opcode) }
    rescue Halt
      @halted = true
      return
    ensure
      debug "Done with execution\n"
    end

    private def execute_instruction(opcode : Opcodes, *modes : ParameterModes)
      debug "Executing #{opcode}, Params: "
      {% begin %}
      case opcode
      {% for opcode in Opcodes.constants.map(&.underscore) %}
      when .{{opcode}}? then {{opcode}}(*modes)
      {% end %}
      end
      {% end %}
      debug "\n"
    end

    private def add(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      result_address = fetch_address(mode_c, program[@ip + 3])

      debug "program[#{result_address}] = #{a + b} = #{a} + #{b}"
      program[result_address] = a + b
    ensure
      @ip += 4
    end

    private def mul(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      result_address = fetch_address(mode_c, program[@ip + 3])

      debug "program[#{result_address}] = #{a * b} = #{a} * #{b}"
      program[result_address] = a * b
    ensure
      @ip += 4
    end

    private def input(mode_a, *other_modes)
      result_address = fetch_address(mode_a, program[@ip + 1])
      input = @input.gets(chomp: true)
      raise "EOF" if input.nil?
      debug "program[#{result_address}] = #{input}"
      program[result_address] = input.to_i64
    ensure
      @ip += 2
    end

    private def output(mode_a, *other_modes)
      @output.puts(*fetch_params(mode_a))
    ensure
      @ip += 2
    end

    private def jump_if_true(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      if a != 0
        debug "jump to #{b}"
        @ip = b.to_u64
      else
        @ip += 3
      end
    end

    private def jump_if_false(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      if a == 0
        debug "jump to #{b}"
        @ip = b.to_u64
      else
        @ip += 3
      end
    end

    private def less_than(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      result_address = fetch_address(mode_c, program[@ip + 3])
      debug "program[#{result_address}] = #{a < b ? 1 : 0} = #{a} < #{b}"
      program[result_address] = a < b ? 1_i64 : 0_i64
    ensure
      @ip += 4
    end

    private def equals(mode_a, mode_b, mode_c)
      a, b = fetch_params(mode_a, mode_b)
      result_address = fetch_address(mode_c, program[@ip + 3])
      debug "program[#{result_address}] = #{a == b ? 1 : 0} = #{a} == #{b}"
      program[result_address] = a == b ? 1_i64 : 0_i64
    ensure
      @ip += 4
    end

    private def adjust_relative_base(mode_a, mode_b, mode_c)
      a = fetch_params(mode_a).first
      debug "adjust relative base by #{a} => #{@relative_base + a}"
      @relative_base += a
    ensure
      @ip += 2
    end

    private def halt(*modes)
      raise Halt.new
    end

    private def fetch_address(mode, value)
      case mode
      when .position? then value
      when .relative? then @relative_base + value
      else raise "Immediate mode not supported for write addresses"
      end
    end

    private def fetch_params(*modes : *T) forall T
      {% begin %}
      program.values_at(
        {% for i in 1..T.size %}
          @ip + {{i}},
        {% end %}
      ).map_with_index do |value, index|
        debug "#{modes[index]} #{value}, "
        case modes[index]
        when .position? then program[value]
        when .immediate? then value
        when .relative? then program[@relative_base + value]
        else raise "#{modes[index]} mode NYI"
        end
      end.tap do |result|
        debug "=> #{result}, "
      end
      {% end %}
    end

    private def parse_opcode
      opcode_string = program[@ip].to_s

      mode_c, mode_b, mode_a = opcode_string[0..-3].rjust(3, '0').chars.map do |mode_char|
        ParameterModes.from_value(mode_char.to_i)
      end

      opcode = Opcodes.from_value(opcode_string.rjust(2, '0')[-2..-1].to_i)

      {opcode, mode_a, mode_b, mode_c}
    end
  end
end
