require "../day"
require "../intcode"

module AOC2019::Days::Day2
  module Shared
    protected def execute_with(noun, verb, intcode = Intcode.new(input))
      intcode.program[1] = noun.to_i64
      intcode.program[2] = verb.to_i64
      intcode.execute
      intcode.program[0]
    end
  end

  class Part1 < Day::Part
    include Shared

    def run
      output.puts execute_with(12, 2)
    end
  end

  class Part2 < Day::Part
    include Shared

    def run
      intcode = Intcode.new(input)

      (0..99).each do |noun|
        (0..99).each do |verb|
          result = execute_with(noun, verb, intcode)
          return output.puts(100 * noun + verb) if result == 19690720
          intcode.reset!
        end
      end
    end
  end
end
