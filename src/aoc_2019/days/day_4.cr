require "../day"

module AOC2019::Days::Day4
  WORKERS = ENV.fetch("CRYSTAL_WORKERS", "4").to_i

  class Iterator
    include ::Iterator(String)

    @current_chars : Array(Char)
    @end_chars : Array(Char)
    @adjacent_range : Range(Int32, Int32)

    def initialize(range_start, range_end, min_adjacent, max_adjacent = Int32::MAX)
      @current_chars = range_start.chars
      @end_chars = range_end.chars
      @adjacent_range = min_adjacent..max_adjacent
    end

    def next
      loop do
        current_group = 'a'
        group_sizes = [] of Int32
        adjacent = false

        @current_chars.size.times do |index|
          a = @current_chars[index]
          b = @current_chars[index + 1]?
          @current_chars.fill(a, index + 1) if b.try(&.< a)
          next if adjacent

          if a != current_group
            if group_sizes.last? == 2
              adjacent = true
              next
            end

            group_sizes << 0
          end

          current_group = a
          group_sizes[-1] += 1
        end

        return stop if @current_chars > @end_chars

        result = @current_chars.join
        @current_chars = result.succ.chars

        next unless group_sizes.any? { |size| @adjacent_range.includes?(size) }

        return result
      end
    end
  end

  module Shared
    def iterators(workers, range_start, range_end, *args)
      range_start = range_start.to_i
      range_end = range_end.to_i
      offset = (range_end - range_start) // workers

      Array.new(workers) do |i|
        slice_start = range_start + i * offset
        slice_end = slice_start + offset
        slice_end = range_end if i == workers - 1

        Iterator.new(slice_start.to_s, slice_end.to_s, *args)
      end
    end

    def run_parallel(workers, *args)
      range_start, range_end = input.gets.not_nil!.split('-')
      counts = Channel(Int32).new(workers)
      iterators(workers, range_start, range_end, *args).each do |iterator|
        spawn { counts.send(iterator.size) }
      end

      Array(Int32).new(workers) { counts.receive }.sum
    end

    def run_alt(min_adjacent = 2, max_adjacent = Int32::MAX)
      adjacent_range = min_adjacent..max_adjacent
      range_start, range_end = input.gets.not_nil!.split('-')
      start_chars = range_start.chars
      end_chars = range_end.chars
      count = 0

      {% begin %}
        {% digits = (env("DIGITS") || "6").to_i %}

        min_chars = [
          {% for digit in 0...digits %}
            start_chars[{{digit}}],
          {% end %}
        ]

        (1...{{digits}}).each do |i|
          min_chars.fill(min_chars[i - 1], i) if min_chars[i] < min_chars[i - 1]
        end

        (min_chars[0]..end_chars[0]).each_with_index do |%digit{0}, %index{0}|
          {% for digit in 1...digits %}
            %indexes{digit} = [
              {% for i in 0...digit %}
                %index{i},
              {% end %}
            ]

            %chars{digit} = [
              {% for i in 0...digit %}
                %digit{i},
              {% end %}
            ]

            %start_char{digit} = %indexes{digit}.all?(&.== 0) ? min_chars[{{digit}}] : %digit{digit - 1}
            %end_char{digit} = %chars{digit} == end_chars[0, {{digit}}] ? end_chars[{{digit}}] : '9'

            (%start_char{digit}..%end_char{digit}).each_with_index do |%digit{digit}, %index{digit}|
          {% end %}
            chars = [
              {% for digit in 0...digits %}
                %digit{digit},
              {% end %}
            ]

            return count if chars > end_chars
            adjacent_counts = chars.uniq.map { |c| chars.count(c) }

            next unless adjacent_counts.any? { |count| adjacent_range.includes?(count) }
            count += 1
          {% for digit in 1...digits %}
            end
          {% end %}
        end
      {% end %}

      count
    end
  end

  class Part1 < Day::Part
    include Shared

    def run
      if ENV["ALT"]?
        output.puts run_alt(2)
      else
        output.puts run_parallel(WORKERS, 2)
      end
    end
  end


  class Part2 < Day::Part
    include Shared

    def run
      if ENV["ALT"]?
        output.puts run_alt(2, 2)
      else
        output.puts run_parallel(WORKERS, 2, 2)
      end
    end
  end
end
