require "../day"

module AOC2019::Days::Day1
  class Part1 < Day::Part
    def run
      output.puts input.each_line.map(&.to_i).map(&.// 3).map(&.- 2).sum
    end
  end

  class Part2 < Day::Part
    def run
      result = input.each_line.map(&.to_i).sum do |mass|
        sum = 0

        loop do
          required_fuel = mass // 3 - 2
          break if required_fuel < 1
          sum += required_fuel
          mass = required_fuel
        end

        sum
      end

      output.puts result
    end
  end
end
