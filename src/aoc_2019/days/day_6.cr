require "../day"

module AOC2019::Days::Day6
  class CelestialBody
    getter name
    getter! orbiting : self?
    protected setter orbiting
    getter in_orbit : Array(self)
    getter level

    def initialize(@name : String, @level : Int32 = 0)
      @in_orbit = [] of self
    end

    def level=(level)
      @level = level
      @in_orbit.each(&.level= level + 1)
    end

    def <<(other : self)
      @in_orbit << other
      other.level = @level + 1
      other.orbiting = self
    end

    def hash
      @name.hash
    end

    def ==(other : self)
      @name == other.name
    end
  end

  module Shared
    def parse_celestial_bodies
      celestial_bodies = Hash(String, CelestialBody).new do |hash, key|
        hash[key] = CelestialBody.new(key)
      end

      input.each_line do |line|
        name_a, name_b = line.split(')')
        celestial_bodies[name_a] << celestial_bodies[name_b]
      end

      celestial_bodies
    end
  end

  class Part1 < Day::Part
    include Shared

    def run
      celestial_bodies = parse_celestial_bodies
      output.puts celestial_bodies.values.sum(&.level)
    end
  end

  class Part2 < Day::Part
    include Shared

    def run
      celestial_bodies = parse_celestial_bodies
      you = celestial_bodies["YOU"].orbiting
      santa = celestial_bodies["SAN"].orbiting

      closer, further = {you, santa}.minmax_by(&.level)
      transfers = 0

      until further.level == closer.level
        further = further.orbiting
        transfers += 1
      end

      until closer == further
        closer = closer.orbiting
        further = further.orbiting
        transfers += 2
      end

      output.puts transfers
    end
  end
end
