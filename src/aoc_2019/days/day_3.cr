require "../day"

module AOC2019::Days::Day3
  struct Point
    getter x : Int32, y : Int32

    def initialize(@x, @y)
    end

    def hash
      {x, y}.hash
    end

    def ==(other : self)
      {x, y} == {other.x, other.y}
    end

    def to_s(io : IO)
      {x, y}.to_s(io)
    end

    def distance_to(other : self)
      (x - other.x).abs + (y - other.y).abs
    end
  end

  struct Range(T)
    forward_missing_to @range

    def initialize(start_value : T, end_value : T)
      if start_value < end_value
        @range = start_value..end_value
      else
        @range = end_value..start_value
      end
    end
  end

  struct Segment
    getter start_point : Point, end_point : Point

    def initialize(@start_point, @end_point)
    end

    def hash
      {start_point, end_point}.hash
    end

    def ==(other : self)
      {start_point, end_point} == {other.start_point, other.end_point}
    end

    def intersection(other : self)
      if start_point.x == end_point.x
        return if other.start_point.x == other.end_point.x
        return unless Range.new(other.start_point.x, other.end_point.x).includes?(start_point.x)
        return unless Range.new(start_point.y, end_point.y).includes?(other.start_point.y)
        Point.new(start_point.x, other.start_point.y)
      else
        return unless other.start_point.x == other.end_point.x
        return unless Range.new(other.start_point.y, other.end_point.y).includes?(start_point.y)
        return unless Range.new(start_point.x, end_point.x).includes?(other.start_point.x)
        Point.new(other.start_point.x, start_point.y)
      end
    end

    def steps
      start_point.distance_to(end_point)
    end

    def to_s(io : IO)
      start_point.to_s(io)
      io << " -> "
      end_point.to_s(io)
    end
  end

  module Shared
    protected def paths
      paths = input.each_line.map(&.split ',').map do |segments|
        position = Point.new(0, 0)
        segments.map do |segment_string|
          parse_segment(position, segment_string).tap do |segment|
            position = segment.end_point
          end
        end
      end

      paths.to_a
    end

    protected def intersections(first, second)
      intersections = Hash(Point, {Segment, Segment}).new

      first.each do |first_path_segment|
        second.each do |second_path_segment|
          intersection = first_path_segment.intersection(second_path_segment)
          next if intersection.nil?
          intersections[intersection] = {first_path_segment, second_path_segment}
        end
      end

      intersections.tap(&.delete Point.new(0, 0))
    end

    private def parse_segment(position, segment)
      chars = segment.chars
      direction = chars.first
      distance = chars[1..-1].join.to_i

      Segment.new(position, end_point_for(position, direction, distance))
    end

    private def end_point_for(position, direction, distance)
      case direction
      when 'U' then Point.new(position.x, position.y + distance)
      when 'L' then Point.new(position.x - distance, position.y)
      when 'D' then Point.new(position.x, position.y - distance)
      when 'R' then Point.new(position.x + distance, position.y)
      else raise "Invalid direction #{direction}"
      end
    end
  end

  class Part1 < Day::Part
    include Shared

    def run
      first, second = paths
      result = intersections(first, second).keys.min_of do |point|
        point.x.abs + point.y.abs
      end

      output.puts result
    end
  end

  class Part2 < Day::Part
    include Shared

    def run
      first, second = paths
      result = intersections(first, second).min_of do |point, (first_segment, second_segment)|
        first_segments = first[0...first.index(first_segment)]
        second_segments = second[0...second.index(second_segment)]

        (first_segments + second_segments).sum(&.steps) +
          first_segment.start_point.distance_to(point) +
          second_segment.start_point.distance_to(point)
      end

      output.puts result
    end
  end
end
