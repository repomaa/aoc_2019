require "../day"

module AOC2019::Days::Day8
  class Row
    include Enumerable(Char)
    include Iterable(Char)

    def initialize(input : IO, width : Int32)
      @buffer = IO::Memory.new(width)
      IO.copy(input, @buffer, width)
    end

    def each
      each.each do |char|
        yield char
      end
    end

    def each
      @buffer.rewind
      @buffer.each_char
    end
  end

  class Layer
    include Iterator(Row)

    def initialize(input : IO, @width : Int32, @height : Int32)
      @buffer = IO::Memory.new(width * height)
      IO.copy(input, @buffer, width * height)
      @buffer.rewind

      @current_row = 0
    end

    def next
      return stop unless @current_row < @height
      @current_row += 1
      Row.new(@buffer, @width)
    end
  end

  class Image
    include Iterator(Layer)

    @layer_count : Int32

    def initialize(input : IO, @width : Int32, @height : Int32)
      @buffer = IO::Memory.new
      IO.copy(input, @buffer)
      @buffer.rewind


      @layer_count = @buffer.size // width // height
      @current_layer = 0
    end

    def next
      return stop unless @current_layer < @layer_count
      @current_layer += 1
      Layer.new(@buffer, @width, @height)
    end
  end

  class Part1 < Day::Part
    def run
      image = Image.new(input, 25, 6)
      char_counts = image.min_of do |layer|
        counts = Hash(Char, Int32).new do |hash, key|
          hash[key] = 0
        end

        layer.each do |row|
          row.each do |digit|
            counts[digit] += 1
          end
        end

        {counts['0'], counts['1'], counts['2']}
      end

      output.puts char_counts[1] * char_counts[2]
    end
  end

  class Part2 < Day::Part
    def run
      flat_image = Array.new(6) do
        Array.new(25, '2')
      end

      image = Image.new(input, 25, 6)

      image.each do |layer|
        layer.each_with_index do |row, y|
          row.each_with_index do |pixel, x|
            next if flat_image[y][x] != '2'
            flat_image[y][x] = pixel
          end
        end
      end

      flat_image.each do |row|
        chars = row.map do |pixel|
          case pixel
          when '0' then '.'
          when '1' then '#'
          end
        end

        output.puts chars.join
      end
    end
  end
end
