require "../day"
require "../intcode"

module AOC2019::Days::Day9
  class Part1 < Day::Part
    def run
      intcode = Intcode.new(input, "1", output: output)
      intcode.execute
    end
  end

  class Part2 < Day::Part
    def run
      intcode = Intcode.new(input, "2", output: output)
      intcode.execute
    end
  end
end
