require "../day"
require "../intcode"

module AOC2019::Days::Day11
  record Point, x : Int32, y : Int32

  class Robot
    enum Directions
      Up
      Left
      Down
      Right
    end

    enum Turns
      Left = 0
      Right = 1
    end

    @input_writer : IO
    @output_reader : IO
    @directions : Deque(Directions)

    def initialize(program_input)
      @position = Point.new(0, 0)
      @directions = Deque.new([
        Directions::Up,
        Directions::Left,
        Directions::Down,
        Directions::Right
      ])
      input_reader, @input_writer = IO.pipe
      @output_reader, output_writer = IO.pipe

      @intcode = Intcode.new(program_input, input_reader, output_writer)
    end

    def paint!(panels)
      spawn @intcode.execute

      loop do
        break if @intcode.halted?
        @input_writer.puts panels[@position]
        panels[@position] = @output_reader.gets.not_nil!.to_i
        turn!(Turns.from_value(@output_reader.gets.not_nil!.to_i))
        move!
      end
    end

    def direction
      @directions.first
    end

    def turn!(direction)
      case direction
      when .left? then @directions.rotate!(1)
      when .right? then @directions.rotate!(-1)
      end
    end

    def move!(amount = 1)
      case direction
      when .up?
        @position = Point.new(@position.x, @position.y - amount)
      when .left?
        @position = Point.new(@position.x - amount, @position.y)
      when .down?
        @position = Point.new(@position.x, @position.y + amount)
      when .right?
        @position = Point.new(@position.x + amount, @position.y)
      end
    end
  end

  class Part1 < Day::Part
    def run
      panels = Hash(Point, Int32).new do |hash, point|
        hash[point] = 0
      end

      robot = Robot.new(input)
      robot.paint!(panels)
      output.puts(panels.size)
    end
  end

  class Part2 < Day::Part
    def run
      panels = Hash(Point, Int32).new do |hash, point|
        hash[point] = 0
      end

      panels[Point.new(0, 0)] = 1
      robot = Robot.new(input)
      robot.paint!(panels)

      print_panels(panels)
    end

    def print_panels(panels)
      top_left, bottom_right = panels.keys.minmax_by { |p| {p.y, p.x} }

      (top_left.y..bottom_right.y).each do |y|
        (top_left.x..bottom_right.x).map { |x| Point.new(x, y) }.each do |point|
          output.print panels[point] == 1 ? '#' : '.'
        end

        output.puts
      end
    end
  end
end
