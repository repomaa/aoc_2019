require "../day"
require "../intcode"

module AOC2019::Days::Day7
  module Shared
    macro included
      class_getter amplifier_count = 5
      class_getter initial_input = 0
    end

    struct WorkerControl
      getter input, output, trigger
      def initialize(@input : IO, @output : IO, @trigger : Channel(Bool))
      end
    end

    def start_worker(reader, writer, trigger, status)
      machine = Intcode.new(input.rewind, reader, writer)
      spawn do
        loop do
          trigger.receive
          machine.reset!
          machine.execute
          status.send(true)
        end
      end
    end

    def try_phase_settings(settings, control, status)
      settings.each_with_index do |setting, index|
        control[index].input.puts(setting)
        control[index].trigger.send(true)
      end

      yield control.first.input
      self.class.amplifier_count.times { status.receive }
      signal = control.last.output.gets(chomp: true)
      raise "Didn't receive signal on output" if signal.nil?

      signal.to_i
    end

    def start_workers
      status = Channel(Bool).new(self.class.amplifier_count)

      control = pipes.each_slice(2).cons(2).map_with_index do |(input, output), index|
        iw, ir = input
        ow, or = output
        trigger = Channel(Bool).new(1)
        start_worker(ir, ow, trigger, status)
        WorkerControl.new(iw, or, trigger)
      end

      {control, status}
    end

    def phase_setting_combinations
      self.class.possible_phase_settings
        .each_combination
        .flat_map(&.each_permutation.uniq)
    end

    def run
      control, status = start_workers

      result = phase_setting_combinations.max_of do |settings|
        try_phase_settings(settings, control, status) do |input|
          input.puts(self.class.initial_input)
        end
      end

      output.puts result
    end

    abstract def pipes : Enumerable(IO)
  end

  class Part1 < Day::Part
    include Shared

    class_getter possible_phase_settings : Array(Int32) = (0..4).to_a

    def pipes
      (self.class.amplifier_count + 1).times.flat_map do
        reader, writer = IO.pipe
        [writer, reader]
      end
    end
  end

  class Part2 < Day::Part
    include Shared

    class_getter possible_phase_settings : Array(Int32) = (5..9).to_a

    def pipes
      pipes = self.class.amplifier_count.times.flat_map do
        reader, writer = IO.pipe
        [writer, reader]
      end

      pipes = pipes.to_a
      # Create feedback loop
      pipes.unshift(pipes[-1])
      pipes.unshift(pipes[-2])

      pipes
    end
  end
end
