require "../day"

module AOC2019::Days::Day10
  module Point
    getter x, y
  end

  macro minmax(a, b)
    ({{a}} < {{b}}) ? { {{a}}, {{b}} } : { {{b}}, {{a}} }
  end

  struct LineIterator
    include Iterator({Int32, Int32})

    @vertical_offset : Int32
    @horizontal_offset : Int32
    @current : {Int32, Int32}
    @x_range : Range(Int32, Int32)
    @y_range : Range(Int32, Int32)

    def initialize(@a : Point, @b : Point)
      @vertical_offset = b.y - a.y
      @horizontal_offset = b.x - a.x
      gcd = {@vertical_offset.gcd(@horizontal_offset), 1}.max
      @vertical_offset //= gcd
      @horizontal_offset //= gcd

      @current = {a.x, a.y}
      @x_range = Range.new(*Day10.minmax(@a.x, @b.x))
      @y_range = Range.new(*Day10.minmax(@a.y, @b.y))
    end

    def next
      @current = {
        @current[0] + @horizontal_offset,
        @current[1] + @vertical_offset
      }

      return stop if @current == {@b.x, @b.y}

      @current
    end
  end

  struct Asteroid
    include Point

    def initialize(@x : Int32, @y : Int32)
    end

    def <=>(other : self)
      {x, y} <=> {other.x, other.y}
    end

    def hash
      {x, y}.hash
    end

    def angle(other : self)
      delta_y = (other.y - y).abs
      delta_x = (other.x - x).abs

      if other.x >= x && other.y < y
        Math.atan(delta_x / delta_y)
      elsif other.x > x && other.y >= y
        Math.atan(delta_y / delta_x) + Math::PI / 2
      elsif other.x <= x && other.y > y
        Math.atan(delta_x / delta_y) + Math::PI
      else
        Math.atan(delta_y / delta_x) + 3 * Math::PI / 2
      end
    end

    def distance(other : self)
      (other.y - y)**2 + (other.x - x)**2
    end

    def ==(other : self)
      {x, y} == {other.x, other.y}
    end

    def blocking_candidates(other : self)
      LineIterator.new(self, other).map { |(x, y)| Asteroid.new(x, y) }
    end
  end

  class AsteroidField
    getter max_x, max_y
    getter asteroids

    def initialize(@asteroids : Set(Asteroid), @max_x : Int32, @max_y : Int32)
    end

    delegate includes?, to: @asteroids

    def station
      @asteroids.max_of do |asteroid|
        visible_asteroid_count = @asteroids.count do |other|
          next if other == asteroid

          asteroid.blocking_candidates(other).none? do |candidate|
            @asteroids.includes?(candidate)
          end
        end

        {visible_asteroid_count, asteroid}
      end
    end

    def self.parse(input)
      asteroids = Set(Asteroid).new
      max_x = max_y = 0

      input.each_line.with_index.each do |line, y|
        max_y = y
        line.each_char.with_index.select(&.first.== '#').each do |(_, x)|
          max_x = x
          asteroids.add(Asteroid.new(x, y))
        end
      end

      new(asteroids, max_x, max_y)
    end
  end

  class Part1 < Day::Part
    def run
      asteroid_field = AsteroidField.parse(input)
      count, _ = asteroid_field.station
      output.puts count
    end
  end

  class Part2 < Day::Part
    def run
      asteroid_field = AsteroidField.parse(input)
      _, station = asteroid_field.station

      asteroids = (asteroid_field.asteroids - Set{station}).to_a
      asteroids_with_angles = asteroids.map do |asteroid|
        {asteroid, station.angle(asteroid)}
      end

      asteroids_with_angles.sort_by! do |(asteroid, angle)|
        {angle, station.distance(asteroid)}
      end

      count = 0

      loop do
        vaporized_indices = [] of Int32
        angles_vaporized = Set(Float64).new

        asteroids_with_angles.each_with_index do |(asteroid, angle), index|
          next if angles_vaporized.includes?(angle)
          vaporized_indices << index
          angles_vaporized.add(angle)

          count += 1
          return output.puts(asteroid.x * 100 + asteroid.y) if count == 200
        end

        vaporized_indices.each do |index|
          asteroids_with_angles.delete_at(index)
        end
      end
    end
  end
end
