require "benchmark"
require "./aoc_api"

module AOC2019
  module Day
    abstract class Part
      protected getter input, output

      def initialize(@input : IO, @output : IO)
      end

      def run
        raise "NYI"
      end

      def self.run(input : IO, output : IO)
        new(input, output).run
      end
    end

    macro included
      def self.run(part)
        memory_used = 0
        time_elapsed = 0.nanoseconds
        output = IO::Memory.new
        input = IO::Memory.new.tap do |io|
          IO.copy(self.input, io)
          io.rewind
        end

        memory_used = Benchmark.memory do
          time_elapsed = Benchmark.realtime do
            case part
            when 1 then Part1.run(input, output)
            when 2 then Part2.run(input, output)
            else raise "Invalid part #{part}"
            end
          end
        end

        STDERR.puts "=" * 36 + " Result " + "=" * 36
        IO.copy(output.rewind, STDOUT)
        STDERR.puts "=" * 80
        STDERR.puts "Memory used: #{pretty_print_memory(memory_used)}"
        STDERR.puts "(Real)time elapsed: #{pretty_print_time(time_elapsed)}"
      end
    end
  end

  module Days
    {% for i in 1..24 %}
      module Day{{i}}
        include Day

        class_getter day = {{i}}
        @@api : AOCApi?

        def self.pretty_print_memory(memory)
          prefixes = ["Ki", "Mi", "Gi"]
          prefix = nil

          while memory > 1024
            prefix = prefixes.shift
            memory /= 1024
          end

          "\%.2f \%sB" \% {memory, prefix}
        end

        def self.pretty_print_time(timespan)
          {% for unit in %w[minute second millisecond nanosecond] %}
            if timespan.total_{{unit.id}}s > 1
              return "\%.2f \%s(s)" \% {timespan.total_{{unit.id}}s, {{unit}}}
            end
          {% end %}
        end

        def self.api
          @@api ||= AOCApi.new(ENV["AOC_SESSION_KEY"])
        end

        def self.input
          if ENV["TEST_INPUT"]?
            STDIN
          else
            api.input({{i}})
          end
        end

        class Part1 < Day::Part
        end

        class Part2 < Day::Part
        end
      end
    {% end %}
  end
end

require "./days/*"
