require "dotenv"
require "./aoc_2019/*"

module AOC2019
  VERSION = "0.1.0"

  def self.run(day, part = 1)
    {% begin %}
      case day
      {% for i in 1..24 %}
      when {{i}} then Days::Day{{i}}.run(part)
      {% end %}
      end
    {% end %}
  end

  def self.generate(day)
    File.open("src/aoc_2019/days/day_#{day}.cr", "w+") do |file|
      file << <<-EOF
      require "../day"

      module AOC2019::Days::Day#{day}
        class Part1 < Day::Part
          def run
            raise "do something with input"
          end
        end

        class Part2 < Day::Part
          def run
            raise "do something with input"
          end
        end
      end
      EOF
    end
  end
end

Dotenv.load
USAGE = "Usage: #{PROGRAM_NAME} <day> <part>"

case ARGV[0]?
when .nil? then abort(USAGE)
when .starts_with?("g") then AOC2019.generate(ARGV[1].to_i)
when ARGV[1]?.nil? then abort(USAGE)
else AOC2019.run(ARGV[0].to_i, ARGV[1].to_i)
end
