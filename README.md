# aoc_2019

Solutions for Advent of Code 2019 in Crystal

## Usage

Build by running `shards build`

Run by running `bin/aoc_2019 <day> <part>`

Inputs are in `input/day_<day>_<part>`

- [Joakim Repomaa](https://github.com/repomaa) - creator and maintainer
